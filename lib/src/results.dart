import 'package:flutter_driver/flutter_driver.dart';

class EmptyCommandResult extends Result {
  const EmptyCommandResult();

  @override
  Map<String, dynamic> toJson() => <String, dynamic>{};

  static EmptyCommandResult fromJson(Map<String, dynamic> json) {
    return const EmptyCommandResult();
  }
}

class StringCommandResult extends Result {
  final String value;

  StringCommandResult(this.value);

  @override
  Map<String, dynamic> toJson() => {"value": value};

  static StringCommandResult fromJson(Map<String, dynamic> json) {
    return StringCommandResult(json["value"]?.toString());
  }
}

class IntCommandResult extends Result {
  final int value;

  IntCommandResult(this.value);

  @override
  Map<String, dynamic> toJson() => {"value": value};

  static IntCommandResult fromJson(Map<String, dynamic> json) {
    return IntCommandResult(json["value"] as int);
  }
}
