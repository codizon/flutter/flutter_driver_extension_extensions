import 'package:flutter_driver/flutter_driver.dart';

class AtFinder extends SerializableFinder {
  final int n;
  final SerializableFinder finder;

  AtFinder(this.n, this.finder);

  @override
  String get finderType => 'At';

  @override
  Map<String, String> serialize() {
    return finder.serialize()
      ..addAll(super.serialize())
      ..addAll({
        "n": "$n",
        "originType": finder.finderType,
      });
  }

  static SerializableFinder deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
  ) {
    final finderParams = Map<String, String>.from(params)
      ..addAll({"finderType": params['originType']});
    return AtFinder(
      int.parse(params["n"]),
      finderFactory.deserializeFinder(finderParams),
    );
  }
}

class LastFinder extends SerializableFinder {
  final SerializableFinder finder;

  LastFinder(this.finder);

  @override
  String get finderType => 'Last';

  @override
  Map<String, String> serialize() {
    return finder.serialize()
      ..addAll(super.serialize())
      ..addAll({
        "originType": finder.finderType,
      });
  }

  static SerializableFinder deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
  ) {
    final finderParams = Map<String, String>.from(params)
      ..addAll({"finderType": params['originType']});
    return LastFinder(
      finderFactory.deserializeFinder(finderParams),
    );
  }
}
