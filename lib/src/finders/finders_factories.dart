import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_driver_extension_extensions/src/finders/finders.dart';
import 'package:flutter_test/flutter_test.dart';

class AtExtension extends FinderExtension {
  @override
  Finder createFinder(
    SerializableFinder finder,
    CreateFinderFactory finderFactory,
  ) {
    final atFinder = (finder as AtFinder);
    final descendantFinder = finderFactory.createFinder(atFinder.finder);
    return _SafeIndexFinder(descendantFinder, atFinder.n);
  }

  @override
  SerializableFinder deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
  ) {
    return AtFinder.deserialize(params, finderFactory);
  }

  @override
  String get finderType => 'At';
}

class _SafeIndexFinder extends ChainedFinder {
  _SafeIndexFinder(Finder parent, this.index) : super(parent);

  final int index;

  @override
  String get description =>
      '${parent.description} (ignoring all but index $index)';

  @override
  Iterable<Element> filter(Iterable<Element> parentCandidates) {
    try {
      return [parentCandidates.elementAt(index)];
    } on RangeError catch (_) {
      return [];
    }
  }
}

class LastExtension extends FinderExtension {
  @override
  Finder createFinder(
    SerializableFinder finder,
    CreateFinderFactory finderFactory,
  ) {
    final atFinder = (finder as LastFinder);
    final descendantFinder = finderFactory.createFinder(atFinder.finder);
    return descendantFinder.last;
  }

  @override
  SerializableFinder deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
  ) {
    return LastFinder.deserialize(params, finderFactory);
  }

  @override
  String get finderType => 'Last';
}

class _SafeLastFinder extends ChainedFinder {
  _SafeLastFinder(Finder parent) : super(parent);

  @override
  String get description => '${parent.description} (ignoring all but last)';

  @override
  Iterable<Element> filter(Iterable<Element> parentCandidates) {
    if (parentCandidates.isNotEmpty) {
      return [parentCandidates.last];
    } else {
      return [];
    }
  }
}
