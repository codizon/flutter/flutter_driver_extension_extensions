import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_driver_extension_extensions/src/commands/commands.dart';
import 'package:flutter_driver_extension_extensions/src/finders/finders.dart';
import 'package:flutter_driver_extension_extensions/src/results.dart';

extension FlutterDriverExtension on FlutterDriver {
  Future<void> longPress(SerializableFinder finder) async {
    await sendCommand(LongPressCommand(finder));
  }

  Future<void> drag(SerializableFinder finder, double dx, double dy) async {
    await sendCommand(DragCommand(finder, dx, dy));
  }

  Future<void> setText(SerializableFinder finder, String text) async {
    await tap(finder);
    await enterText(text);
  }

  Future<void> clearText(SerializableFinder finder) async {
    await setText(finder, "");
  }

  Future<String> getValueKey(SerializableFinder finder) async {
    final result =
        await sendCommand(GetStringPropertyCommand(finder, 'valueKey'));
    return StringCommandResult.fromJson(result).value;
  }

  Future<String> getInnerText(SerializableFinder finder) async {
    final result =
        await sendCommand(GetStringPropertyCommand(finder, 'innerText'));
    return StringCommandResult.fromJson(result).value;
  }

  Future<int> getCount(SerializableFinder finder) async {
    final result = await sendCommand(GetIntPropertyCommand(finder, 'count'));
    return IntCommandResult.fromJson(result).value;
  }

  Future<int> getChildrenCount(SerializableFinder finder) async {
    final result =
        await sendCommand(GetIntPropertyCommand(finder, 'childrenCount'));
    return IntCommandResult.fromJson(result).value;
  }
}

extension FinderExtension on CommonFinders {
  SerializableFinder at(int n, SerializableFinder finder) {
    return AtFinder(n, finder);
  }
}

extension SerializableFinderExtension on SerializableFinder {
  SerializableFinder at(int n) {
    return AtFinder(n, this);
  }

  SerializableFinder atFirst() => at(0);

  SerializableFinder atLast() {
    return LastFinder(this);
  }

  SerializableFinder descendantOf(SerializableFinder of) {
    return find.descendant(of: of, matching: this);
  }

  SerializableFinder ancestorOf(SerializableFinder of) {
    return find.ancestor(of: of, matching: this);
  }
}
