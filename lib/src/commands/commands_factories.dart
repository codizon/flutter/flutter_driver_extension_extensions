import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_driver_extension_extensions/src/commands/commands.dart';
import 'package:flutter_driver_extension_extensions/src/results.dart';
import 'package:flutter_test/flutter_test.dart';

class LongPressExtension extends CommandExtension {
  @override
  Future<Result> call(
    Command command,
    WidgetController prober,
    CreateFinderFactory finderFactory,
    CommandHandlerFactory handlerFactory,
  ) async {
    final longPressCommand = (command as LongPressCommand);
    final computedFinder = await handlerFactory.waitForElement(
      finderFactory.createFinder(longPressCommand.finder).hitTestable(),
    );
    await prober.longPress(computedFinder);
    return EmptyCommandResult();
  }

  @override
  String get commandKind => 'longPress';

  @override
  Command deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
    DeserializeCommandFactory commandFactory,
  ) {
    return LongPressCommand(finderFactory.deserializeFinder(params));
  }
}

class DragExtension extends CommandExtension {
  @override
  Future<Result> call(
    Command command,
    WidgetController prober,
    CreateFinderFactory finderFactory,
    CommandHandlerFactory handlerFactory,
  ) async {
    final dragCommand = (command as DragCommand);
    final computedFinder = await handlerFactory.waitForElement(
      finderFactory.createFinder(dragCommand.finder).hitTestable(),
    );
    await prober.drag(computedFinder, Offset(dragCommand.dx, dragCommand.dy));
    return EmptyCommandResult();
  }

  @override
  String get commandKind => 'drag';

  @override
  Command deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
    DeserializeCommandFactory commandFactory,
  ) {
    return DragCommand(
      finderFactory.deserializeFinder(params),
      double.parse(params['dx']),
      double.parse(params['dy']),
    );
  }
}

class GetStringPropertyExtension extends CommandExtension {
  @override
  Future<Result> call(
    Command command,
    WidgetController prober,
    CreateFinderFactory finderFactory,
    CommandHandlerFactory handlerFactory,
  ) async {
    final getStringProperty = (command as GetStringPropertyCommand);
    final finder = finderFactory.createFinder(getStringProperty.finder);
    final elements = await handlerFactory.waitForElement(finder);
    final element = elements.evaluate().single;
    switch (getStringProperty.property) {
      case 'valueKey':
        return StringCommandResult(_getValueKey(element));
      case 'innerText':
        return StringCommandResult(_getInnerText(element) ?? "");
    }

    throw UnsupportedError(
        'Property ${getStringProperty.property} is currently not supported by getStringProperty');
  }

  String _getValueKey(Element element) {
    final key = element.widget.key;
    if (key is ValueKey) {
      return key.value?.toString();
    } else {
      throw UnsupportedError(
          'Key ${key.runtimeType.toString()} is currently not supported by getValueKey');
    }
  }

  String _getInnerText(Element element) {
    final widget = element.widget;

    if (widget.runtimeType == Text) {
      return (widget as Text).data;
    } else if (widget.runtimeType == RichText) {
      final richText = widget as RichText;
      if (richText.text.runtimeType == TextSpan) {
        return (richText.text as TextSpan).text;
      }
    } else if (widget.runtimeType == TextField) {
      return (widget as TextField).controller?.text;
    } else if (widget.runtimeType == TextFormField) {
      return (widget as TextFormField).controller?.text;
    } else if (widget.runtimeType == EditableText) {
      return (widget as EditableText).controller.text;
    } else {
      final result = <String>[];
      element.visitChildren((child) {
        final text = _getInnerText(child);
        if (text != null && text != "") {
          result.add(text);
        }
      });
      if (result.isNotEmpty) return result.join(" ");
    }

    return null;
  }

  @override
  String get commandKind => 'getStringProperty';

  @override
  Command deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
    DeserializeCommandFactory commandFactory,
  ) {
    return GetStringPropertyCommand(
        finderFactory.deserializeFinder(params), params['property']);
  }
}

class GetIntPropertyExtension extends CommandExtension {
  @override
  Future<Result> call(
    Command command,
    WidgetController prober,
    CreateFinderFactory finderFactory,
    CommandHandlerFactory handlerFactory,
  ) async {
    final getStringProperty = (command as GetIntPropertyCommand);
    final finder = finderFactory.createFinder(getStringProperty.finder);
    final elementsFinder = await handlerFactory.waitForElement(finder);
    final elements = elementsFinder.evaluate();
    switch (getStringProperty.property) {
      case 'count':
        return IntCommandResult(_getCount(elements));
      case 'childrenCount':
        return IntCommandResult(await _getChildrenCount(elements));
    }

    throw UnsupportedError(
        'Property ${getStringProperty.property} is currently not supported by getIntProperty');
  }

  int _getCount(Iterable<Element> elements) {
    return elements.length;
  }

  Future<int> _getChildrenCount(Iterable<Element> elements) async {
    final element = elements.single;
    var counter = 0;
    element.visitChildren((element) {
      counter++;
    });
    return counter;
  }

  @override
  String get commandKind => 'getIntProperty';

  @override
  Command deserialize(
    Map<String, String> params,
    DeserializeFinderFactory finderFactory,
    DeserializeCommandFactory commandFactory,
  ) {
    return GetIntPropertyCommand(
        finderFactory.deserializeFinder(params), params['property']);
  }
}
