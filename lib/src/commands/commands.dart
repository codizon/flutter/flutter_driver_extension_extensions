import 'package:flutter_driver/flutter_driver.dart';

class LongPressCommand extends CommandWithTarget {
  LongPressCommand(SerializableFinder finder) : super(finder);

  @override
  String get kind => 'longPress';
}

class DragCommand extends CommandWithTarget {
  final double dx;
  final double dy;

  DragCommand(SerializableFinder finder, this.dx, this.dy) : super(finder);

  @override
  String get kind => 'drag';

  @override
  Map<String, String> serialize() {
    return super.serialize()
      ..addAll({
        "dx": "$dx",
        "dy": "$dy",
      });
  }
}

class GetStringPropertyCommand extends CommandWithTarget {
  final String property;

  GetStringPropertyCommand(SerializableFinder finder, this.property)
      : super(finder);

  @override
  String get kind => 'getStringProperty';

  @override
  Map<String, String> serialize() {
    return super.serialize()..addAll({"property": property});
  }
}

class GetIntPropertyCommand extends CommandWithTarget {
  final String property;

  GetIntPropertyCommand(SerializableFinder finder, this.property)
      : super(finder);

  @override
  String get kind => 'getIntProperty';

  @override
  Map<String, String> serialize() {
    return super.serialize()..addAll({"property": property});
  }
}
