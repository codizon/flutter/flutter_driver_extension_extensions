import 'package:flutter_driver_extension_extensions/src/commands/commands_factories.dart';
import 'package:flutter_driver_extension_extensions/src/finders/finders_factories.dart';

final commandExtensions = [
  LongPressExtension(),
  DragExtension(),
  GetStringPropertyExtension(),
  GetIntPropertyExtension(),
];

final finderExtensions = [
  AtExtension(),
  LastExtension(),
];
