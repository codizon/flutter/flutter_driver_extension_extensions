import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_driver_extension_extensions/flutter_driver_extensions.dart';
import 'package:test/test.dart';

void main() async {
  FlutterDriver driver;

  SerializableFinder _submit() => find.byValueKey("submit");
  SerializableFinder _lastAction() => find.byValueKey("last_action");
  SerializableFinder _input() => find.byValueKey("input");
  SerializableFinder _column() => find.byValueKey("column");
  SerializableFinder _draggable() => find.byValueKey("draggable");

  group("E2E Tests", () {
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      await driver.close();
    });

    group("commands", () {
      group("actions", () {
        test("tap", () async {
          await driver.tap(_submit());
          expect(await driver.getText(_lastAction()), "tap");
        });

        test("longPress", () async {
          await driver.longPress(_submit());
          expect(await driver.getText(_lastAction()), "longPress");
        });

        test("drag", () async {
          await driver.drag(_draggable(), 2, 3);
          expect(await driver.getText(_lastAction()), "onDragUpdate 2/3");
        });

        test("setText", () async {
          final text = "abc";
          await driver.setText(_input(), text);
          expect(await driver.getText(_input()), text);
        });

        test("clearText", () async {
          await driver.setText(_input(), "abc");
          await driver.clearText(_input());
          expect(await driver.getText(_input()), "");
        });
      });

      group("getters", () {
        test("getValueKey", () async {
          expect(await driver.getValueKey(_input()), "input");
          expect(await driver.getValueKey(_submit()), "submit");
        });

        test("getCount", () async {
          final count = await driver.getCount(find.text("Button"));
          expect(count, 3);
        });

        test("getChildrenCount group", () async {
          final count = await driver.getChildrenCount(_column());
          expect(count, 3);
        });

        test("getChildrenCount input", () async {
          final count = await driver.getChildrenCount(_input());
          expect(count, 1);
        });

        test("getInnerText for single element", () async {
          final innerText = await driver.getInnerText(_submit());
          expect(innerText, "Submit");
        });

        test("getInnerText for group", () async {
          final innerText = await driver.getInnerText(_column());
          expect(innerText, "Button Button Button");
        });
      });
    });

    group("finders", () {
      test("at", () async {
        final buttonFinder = find.text("Button");
        expect(await driver.getValueKey(buttonFinder.at(0)), "button_label1");
        expect(await driver.getValueKey(buttonFinder.at(1)), "button_label2");
        expect(await driver.getValueKey(buttonFinder.at(2)), "button_label3");
      });

      test("atFirst", () async {
        final buttonFinder = find.text("Button");
        expect(
            await driver.getValueKey(buttonFinder.atFirst()), "button_label1");
      });

      test("atLast", () async {
        final buttonFinder = find.text("Button");
        expect(
            await driver.getValueKey(buttonFinder.atLast()), "button_label3");
      });
    });
  });
}
