// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_driver_extension_extensions/flutter_driver_extension_factories.dart';

void main() {
  // ignore: close_sinks
  enableFlutterDriverExtension(
    commands: commandExtensions,
    finders: finderExtensions,
    silenceErrors: true, // std errors doesn't work on Flutter
  );
  timeDilation = 0.01; // Will speed up animations
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(
        title: "Demo",
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, @required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _lastAction = "";
  TextEditingController textController = TextEditingController();

  void _setLastAction(String action) {
    setState(() {
      _lastAction = action;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: textController,
              key: Key("input"),
            ),
            MaterialButton(
              key: Key("submit"),
              child: Text("Submit"),
              onPressed: () {
                _setLastAction("tap");
              },
              onLongPress: () {
                _setLastAction("longPress");
              },
            ),
            Text(
              '$_lastAction',
              key: Key("last_action"),
            ),
            Row(
              children: [
                Draggable(
                  key: Key("draggable"),
                  child: Text("Draggable"),
                  feedback: Text("Dragged!"),
                  onDragUpdate: (d) {
                    _setLastAction("onDragUpdate ${d.delta.dx}/${d.delta.dy}");
                  },
                ),
              ],
            ),
            Column(
              key: Key("column"),
              children: [
                MaterialButton(
                  key: Key("button1"),
                  child: Text(
                    "Button",
                    key: Key("button_label1"),
                  ),
                  onPressed: () {},
                ),
                MaterialButton(
                  key: Key("button2"),
                  child: Text(
                    "Button",
                    key: Key("button_label2"),
                  ),
                  onPressed: () {},
                ),
                MaterialButton(
                  key: Key("button3"),
                  child: Text(
                    "Button",
                    key: Key("button_label3"),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }
}
