# flutter_driver_extension_extensions

Flutter package to enhance Flutter Driver possibilities with [Finder Extensions](https://api.flutter.dev/flutter/flutter_driver_extension/FinderExtension-class.html) and [Command Extensions](https://api.flutter.dev/flutter/flutter_driver_extension/CommandExtension-class.html).

## Rationale

Flutter Driver enables test instrumentation from _any_ language or framework. And so this extensions.
The case is different for [integration tests package](https://flutter.dev/docs/testing/integration-tests), which only allows test cases to be written from dart (in general). 

## Getting Started

Assuming you're already using Flutter Driver, you can go straight to point 2.

### 1. Turn On Flutter Driver

Follow [an introduction to integration testing](https://flutter.dev/docs/cookbook/testing/integration/introduction) steps.

### 2. Attach Flutter Driver Extensions

In `test_driver/app.dart` (or any your test driver application file) add import:

```dart
import 'package:flutter_driver_extension_extensions/flutter_driver_extension_factories.dart';
```

In the same file, call `enableFlutterDriverExtension` with the new `command` and `finders` arguments:

```dart
enableFlutterDriverExtension(
    // +++
    commands: commandExtensions,
    finders: finderExtensions,
    // +++
);
```

### 3. Use Extensions From Tests File

In `test_driver/app_test.dart` add import:

```dart
import 'package:flutter_driver_extension_extensions/flutter_driver_extensions.dart';
```

And start using extensions:

```
group("E2E Tests", () {
    setUpAll(() async {
        driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
        await driver.close();
    });

    test("longPress", () async {
        await driver.longPress(find.byValueKey("button"));
    });

    test("getChildrenCount", () async {
        final count = await driver.getChildrenCount(find.byValueKey("row"));
        expect(count, 7);
    });

    test("atFirst finder", () async {
        final count = await driver.waitFor(find.byType("MaterialButton").atFirst());
        expect(count, 7);
    });
}
```

## Available extensions

The package adds extension functions on Driver object and finders.
> `driver` is the object returned by `driver = await FlutterDriver.connect();`
> `finder` is one of `find.byText(...)`, `find.byType(...)` etc. Could be used as an argument of command (e.g. waitFor)

Commands:

- `driver.getValueKey(finder)` - returns a `Key` value of single element found by `finder` (tip: it must be only one available)
- `driver.getCount(finder)` - returns a number of elements matched by the given `finder` (e.g. number of MaterialButtons in the row)
- `driver.getChildrenCount(finder)` - returns a number of direct children inside a component found by `finder`
- `driver.getInnerText(finder)` - returns a "text" of any component that has children with 

Finders:

- `finder.at(n)` - finds an element at `n`th place
- `finder.atFirst()` - finds only first element
- `finder.atLast()` - finds the last element

For the complete use case check [our E2E tests](https://gitlab.com/codizon/flutter/flutter_driver_extension_extensions/-/tree/master/test_driver).