## [0.0.4] - 9.04.2021

* Document public members.

## [0.0.3] - 9.04.2021

* Improve sample.

## [0.0.3] - 9.04.2021

* Add example.
  
## [0.0.2] - 8.04.2021

* Add readme.

## [0.0.1] - 5.04.2021

* Initial release.
